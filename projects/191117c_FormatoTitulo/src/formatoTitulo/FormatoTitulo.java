package formatoTitulo;

import java.util.Scanner;

public class FormatoTitulo {
	public static String formatoTitulo(String frase) {
		String fraseOut = "";
		int i = 0;
		char c = ' ';
		
		for (i = 0; i < frase.length(); i++) {
			c = frase.charAt(i);
			if (Character.isLetter(c) && (i == 0 || frase.charAt(i - 1) == ' ')) fraseOut += Character.toUpperCase(c);
				else fraseOut += c;
		}
		
		return fraseOut;
	}
	
	public static void main (String[] args) {
		System.out.print("Escribe una frase: ");
		Scanner s = new Scanner(System.in);
		System.out.print(formatoTitulo(s.nextLine()));
		s.close();
	}
}
