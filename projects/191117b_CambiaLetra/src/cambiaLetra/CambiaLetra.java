package cambiaLetra;

import java.util.Scanner;

public class CambiaLetra {
	public static String cambiaLetra(String frase) {
		String fraseOut = "";
		char vocal[] = {'a', 'e', 'i', 'o', 'u'};
		int i = 0, j = 0;
		char c = ' ';
		
		for (i = 0; i < frase.length(); i++) {
			c = frase.charAt(i);
			if (Character.isLetter(c)) {
				c++;
				for (j = 0; j < vocal.length; j++)
					if (vocal[j] == c) c = Character.toUpperCase(c);
				fraseOut += c;
			} else fraseOut += c;
		}
		
		return fraseOut;
	}
	
	public static void main (String[] args) {
		System.out.print("Escribe una frase: ");
		Scanner s = new Scanner(System.in);
		System.out.print(cambiaLetra(s.nextLine()));
		s.close();
	}
}
