package ajedrezVariable;

import java.util.Scanner;

public class AjedrezVariable {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int i = 0, e = 0, lado = 0;
        char c = 'x';
        char m[][];
        
        // preguntamos el lado
        System.out.print("Tamaño del lado: ");
        lado = s.nextInt();
        
        m = new char[lado][lado];
        
        // llenamos la matriz con basura
        for (i = 0; i < lado; i++) 
            for (e = 0; e < lado; e++)
                m[i][e] = '*';
                
        // llenamos la matriz bien
        for (i = 0; i < lado; i++) {
            for (e = 0; e < lado; e++) {
                if (c == 'x')  c = ' ';
                	else c = 'x';
                m[i][e] = c;
            }
            if (c == 'x') c = ' ';
                else c = 'x';
        }
        
        // visualizamos la matriz
        for (i = 0; i < lado; i++) {
            for (e = 0; e < lado; e++) System.out.print(m[i][e] + " ");
            System.out.println();
        }
        
        s.close();
    }
}
