package palabraLarga;

import java.util.Scanner;

public class PalabraLarga {
	public static String palabraLarga(String frase) {
		String palabra = "", palabramax = "";
		int i = 0, ipalabra = 0, tammax = 0;
		char c = ' ';
		
		for (i = 0; i < frase.length(); i++) {
			c = frase.charAt(i);
			if (Character.isLetter(c)) {
				palabra += c;
				ipalabra++;
			}
			
			if (!Character.isLetter(c) || (i + 1) == frase.length()) {
				if (ipalabra > tammax) {
					palabramax = palabra;
					tammax = ipalabra;
				}
				palabra = "";
				ipalabra = 0;
			}
		}
		return palabramax;
	}
	
	public static void main (String[] args) {
		System.out.print("Escribe una frase: ");
		Scanner s = new Scanner(System.in);
		System.out.print(palabraLarga(s.nextLine()));
		s.close();
	}
}
