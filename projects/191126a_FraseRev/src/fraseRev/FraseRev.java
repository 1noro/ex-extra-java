package fraseRev;

import java.util.Scanner;

public class FraseRev {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int i = 0;
		String fraseIn = "", fraseOut = "";
		String palabra = "";
		char c = ' ';
		
		System.out.print("Frase: ");
		fraseIn = s.nextLine();
		
		for (i = 0; i < fraseIn.length(); i++) {
			c = fraseIn.charAt(i);
			if (c != ' ') {
				palabra += c; 
			}
			if (c == ' ' || i == (fraseIn.length() - 1)) {
				fraseOut = palabra + " " + fraseOut;
				palabra = "";
			}
		}
		
		System.out.println(fraseOut);
		s.close();
		
	}

}
