package conversorHorasMinutos;

import java.io.IOException;
import java.util.Scanner;

public class ConversorHorasMinutos {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		char entrada = ' ';
		int h = 0, m = 0, _h = 0, _m = 0;
		do {
			
			do {
				System.out.print("Introducir horas (h), minutos (m), salir (x): ");
				try {
					entrada = (char) System.in.read();
					while(System.in.read()!='\n'); //leer hasta pulsar enter '\n'
				} catch (IOException ioe) {System.out.println("error1");}
				entrada = Character.toLowerCase(entrada);
			} while (entrada != 'h' && entrada != 'm' && entrada != 'x');
			
			if (entrada != 'x') {
				
				if (entrada == 'h') {
					do {
						System.out.print("Introduce horas: ");
						h = s.nextInt();
					} while (h < 0);
					_m = h * 60;
					System.out.println("Resultado: " + _m + " minutos");
				}
				
				if (entrada == 'm') {
					do {
						System.out.print("Introduce minutos: ");
						m = s.nextInt();
					} while (m < 0);
					_h = m/60;
					_m = m - (60 * _h);
					System.out.println("Resultado: " + _h + " horas y " + _m + " minutos");
				}
				
			}
			
		} while (entrada != 'x');

		s.close();
	}

}
