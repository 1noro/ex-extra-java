package ahorcado;
import java.util.Scanner;
import java.io.IOException;
public class Ahorcado {
	public static void main (String[]args) {
		Scanner e = new Scanner(System.in);
		String palabra="" ;
		int i=0, tam=0, contM=1;
		char letra = ' ';
		char vPalabra[], vBien[], vMal[];
		boolean encontrado = false;
		System.out.println("Palabra?");
		palabra = e.nextLine();

		tam = palabra.length();
		vPalabra = new char [tam];
		vBien = new char[tam];
		vMal = new char [tam];

		vPalabra=palabra.toCharArray(); //SOBRE UN STRING LLEVA PARENTESIS; SOBRE ARRAY NO
		//LLENADO ARRAYS
		for (i=0;i<tam;i++) {
			if (i==0 || i==tam-1){
				vBien[i]=vPalabra[i];
				vMal[i]=vPalabra[i];
			}
			else {
				vBien[i]='_';
				vMal[i]='_';
			}
		}
		
		vPalabra[0] = '_';
		vPalabra[tam-1] = '_';
		do {
			encontrado=false;
			//VISUALIZACIÓN ARRAY BIEN
			System.out.print("vPalabra: ");
			for (i=0;i<tam;i++) {
				System.out.print(vPalabra[i]+" ");
			}
			System.out.println("");
			
			//VISUALIZACIÓN ARRAY BIEN
			System.out.print("Aciertos: ");
			for (i=0;i<tam;i++) {
				System.out.print(vBien[i]+" ");
			}
			System.out.println("");
			
			//VISUALIZACIÓN ARRAY MAL
			System.out.print("Fallos:   ");
			for (i=0;i<tam;i++) {
				System.out.print(vMal[i]+" ");
			}
			//COMPARACIÓN
			System.out.println("");
			
			System.out.println("letra?");
			try {
				letra= (char) System.in.read();
				while(System.in.read()!='\n');
			}catch (IOException ioe) {}

			letra = Character.toLowerCase(letra);
			
			for(i=0;i<tam;i++) {
				if (letra==vPalabra[i]){
					encontrado=true;
					vBien[i]=letra;
					vPalabra[i] = '_';
				}
			}
			if (encontrado == false) {
				
				vMal[contM]=letra;
				contM++;
			}
			
			encontrado=false;
			for (i=0;i<tam;i++) {
				if (vBien[i]=='_') {
					encontrado = true;
				}		
			}
			
		} while (contM != tam-1 && encontrado == true);
		if (contM==tam-1) {
			System.out.println("Perdiste");
		}
		else {
			System.out.println("Ganaste");
		}
		e.close();
	}
}
