package palabraLarga;

import java.util.Scanner;

public class PalabraLarga {

	public static void main (String[] args) {
		System.out.print("Escribe una frase: ");
		Scanner s = new Scanner(System.in);
		String frase = "";
		frase = s.nextLine();
		
		String palabra = "", palabraMax = "";
		int i = 0, iPalabra = 0, tamMax = 0;
		char c = ' ';
		
		for (i = 0; i < frase.length(); i++) {
			c = frase.charAt(i);
			if (Character.isLetter(c)) {
				palabra += c;
				iPalabra++;
			}
			
			if (!Character.isLetter(c) || (i + 1) == frase.length()) {
				if (iPalabra > tamMax) {
					palabraMax = palabra;
					tamMax = iPalabra;
				}
				palabra = "";
				iPalabra = 0;
			}
		}
		
		System.out.println("Mas larga: " + palabraMax);
		
		s.close();
	}
}
