package ordenarPalabra;

import java.util.Scanner;

public class OrdenarPalabra {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int i = 0, e = 0, tam = 0;
        String palabra = "";
        char vPalabra[];
        char aux = ' ';
        
        System.out.print("Palabra: ");
        palabra = s.next();
        
        vPalabra = palabra.toCharArray();
        tam = palabra.length();
        
        for (i = 0; i < (tam - 1); i++) {
            for (e = (i + 1) ; e < tam; e++) {
                if (vPalabra[i] > vPalabra[e]) {
                    aux = vPalabra[i];
                    vPalabra[i] = vPalabra[e];
                    vPalabra[e] = aux;
                }
            }
        }
        
        System.out.println(vPalabra);
        s.close();
    }
}
