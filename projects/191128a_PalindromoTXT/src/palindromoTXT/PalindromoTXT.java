package palindromoTXT;

import java.util.Scanner;

public class PalindromoTXT {
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int i = 0, tam = 0, tamNS = 0;
        String fraseIn = "", fraseNoSpaces = "";
        char c = ' ';
        boolean esPalindromo = true;
        
        System.out.print("Frase: ");
        fraseIn = s.nextLine();
        
        tam = fraseIn.length();
        
        for (i = 0; i < tam; i++) {
            c = fraseIn.charAt(i);
            if (c != ' ') fraseNoSpaces += c;
        }
        
        tamNS = fraseNoSpaces.length();
        
        for (i = 0; i < tamNS; i++) {
            if (fraseNoSpaces.charAt(i) != fraseNoSpaces.charAt((tamNS - 1) - i))
                esPalindromo = false;
        }
        
        if (esPalindromo) System.out.println("Es palindromo");
            else System.out.println("NO es palindromo");
            
        s.close();
        
    }
    
}
